import React, { useState } from "react";

function AddItem(props) {
  const [name, setName] = useState("");
  const [price, setPrice] = useState(0);

  return (
    <form
      className="row mb-5"
      onSubmit={(e) => {
        e.preventDefault();
        props.addItem(name, Number(price));
      }}
    >
      <div className="mb-3 col-4">
        <label htmlFor="inputName" className="form-label">
          Name
        </label>
        <input
          type="text"
          className="form-control"
          id="inputName"
          aria-describedby="name"
          name="productName"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
      </div>

      <div className="mb-3 col-4">
        <label htmlFor="inputPrice" className="form-label">
          Price
        </label>
        <input
          type="number"
          className="form-control"
          id="price"
          name="productPrice"
          value={price}
          onChange={(e) => setPrice(e.target.value)}
        />
      </div>
      <button type="submit" className="btn btn-primary col-4">
        Add
      </button>
    </form>
  );
}

export default AddItem;
