import React from "react";

import Product from "./Product";

function ProductList(props) {
  return props.productList.length > 0 ? (
    props.productList.map((product, index) => {
      return (
        <Product
          key={index}
          index={index}
          id={product.id}
          product={product}
          incrementQuantity={props.incrementQuantity}
          decrementQuantity={props.decrementQuantity}
          removeQuantity={props.removeQuantity}
        />
      );
    })
  ) : (
    <h1 className="text-dark ">No products exists in the cart</h1>
  );
}

export default ProductList;
