import React from "react";

class Navbar extends React.Component {
  render(props) {
    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
          <a className="navbar-brand" href="#">
            Shopping Cart
            <span className="badge rounded-pill bg-danger">
              {this.props.productCount}
            </span>
          </a>
        </div>
      </nav>
    );
  }
}

export default Navbar;
