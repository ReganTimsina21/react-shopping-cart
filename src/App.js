import React, { useState, useEffect } from "react";

import Navbar from "./components/common/Navbar";
import Footer from "./components/common/Footer";
import AddItem from "./components/common/AddItem";
import ProductList from "./components/common/ProductList";

import "./App.css";

function App() {
  const PRODUCTLIST = [
    {
      id: 1,
      price: 22,
      name: "IPhone 10s Max",
      quantity: 0,
    },
    {
      id: 2,
      price: 11,
      name: "Redmi Note 10s Max",
      quantity: 0,
    },
  ];

  const [productList, setProductList] = useState(PRODUCTLIST);
  const [totalAmount, setTotalAmount] = useState(0);

  const addItem = (name, price) => {
    let newProductList = [...productList];
    newProductList.push({
      name: name,
      price: price,
      quantity: 0,
    });

    setProductList(newProductList);

    // e.preventDefault();
  };

  const incrementQuantity = (index) => {
    let newProductList = [...productList];
    let newTotalAmount = totalAmount;
    newProductList[index].quantity++;
    newTotalAmount += newProductList[index].price;
    setTotalAmount(newTotalAmount);
    setProductList(newProductList);
  };

  const decrementQuantity = (index) => {
    let newProductList = [...productList];
    let newTotalAmount = totalAmount;
    if (newProductList[index].quantity > 0) {
      newProductList[index].quantity--;
      newTotalAmount -= newProductList[index].price;
      setTotalAmount(newTotalAmount);
    } else {
      newProductList[index].quantity = 0;
    }
    setProductList(newProductList);
  };

  const removeQuantity = (index) => {
    let newProductList = [...productList];
    let newTotalAmount = totalAmount;

    newTotalAmount -=
      newProductList[index].quantity * newProductList[index].price;

    newProductList.splice(index, 1);

    setTotalAmount(newTotalAmount);
    setProductList(newProductList);
  };

  const resetHandler = () => {
    let newProductList = [...productList];

    newProductList.map((obj) => {
      obj.quantity = 0;
    });

    setTotalAmount(0);
    setProductList(newProductList);
  };

  return (
    <>
      <Navbar productCount={productList.length} />

      <main className="container mt-5">
        <AddItem addItem={addItem} />

        <ProductList
          productList={productList}
          incrementQuantity={incrementQuantity}
          decrementQuantity={decrementQuantity}
          removeQuantity={removeQuantity}
        />
      </main>

      <Footer totalAmount={totalAmount} resetHandler={resetHandler} />
    </>
  );
}

export default App;
